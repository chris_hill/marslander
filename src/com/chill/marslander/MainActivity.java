/*
 * Author: Chris Hill 
 * Class: MainAcitivity
 * Description: This class is the main class for the application and defines the widgets used
 * and user interaction
 */

package com.chill.marslander;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements OnTouchListener, Runnable {

	//global variables
	Game gView;
	ImageButton lThrust;
	ImageButton rThrust;
	ImageButton mThrust;
	ImageButton reset;
	ProgressBar fuelGauge;
	ToggleButton mute;
	TextView score;
	
	MediaPlayer mThrustSound;
	MediaPlayer sThrustSound;
	
	float fuel;		//keeps track of fuel
	//static int finalScore;	//keeps track of score
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//remove title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		//define instance GameView class
		gView = (Game) findViewById(R.id.gameArea);
		
		//fuel is set to 100
		fuel = 100;
		//reference to the fuel gauge
		fuelGauge = (ProgressBar) findViewById(R.id.fuelGauge);
		
		//score = (TextView) findViewById(R.id.score);
		//set sound for main thruster
		mThrustSound = MediaPlayer.create(this, R.raw.mainthrustersound);
		//set sound for secondary thruster
		sThrustSound = MediaPlayer.create(this, R.raw.secondarythrusters);
		
		//reference for the mute button
		mute = (ToggleButton) findViewById(R.id.muteTogBtn);
		//set on change listener
		mute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		    	//volume is muted
		        if (isChecked) {
		        	mThrustSound.setVolume(0, 0);
		        	sThrustSound.setVolume(0, 0);
		        	gView.checkMute(true);
		        }
		        //volume is on
		        else {
		        	mThrustSound.setVolume(1, 1);
		        	sThrustSound.setVolume(1, 1);
		        	gView.checkMute(false);
		        }
		    }
		});
		
		//reference to reset button
		reset = (ImageButton) findViewById(R.id.resetBtn);
		reset.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				//reset method is called from the game class
				gView.reset();
				//game class object is redrawn
				gView.invalidate();
				//fuel is set back to 100
				fuel = 100;
				//fuel gauge is updated
				fuelGauge.setProgress((int)fuel);
				
			}
			
		});
		
		//reference to the left thruster button
		lThrust = (ImageButton) findViewById(R.id.leftThrustBtn);
		lThrust.setOnTouchListener(new View.OnTouchListener(){

			private Handler lHandler;
			
			@Override public boolean onTouch(View v, MotionEvent event) {
		        switch(event.getAction()) {
		        case MotionEvent.ACTION_DOWN:
		            if (lHandler != null) return true;
		            lHandler = new Handler();
		            //performs lAction
		            lHandler.post(lAction);
		            //garbage collection
		            System.gc();
		            System.gc();
		            break;
		        case MotionEvent.ACTION_UP:
		            if (lHandler == null) return true;
		            lHandler.removeCallbacks(lAction);
		            //handler set to null
		            lHandler = null;
		            sThrustSound.pause();
					//sThrustSound.release();
					//sThrustSound = null;
		            
		            //garbage collection
		            System.gc();
		            System.gc();
		            break;
		        }
		        return true;
		    }
			//on down
			Runnable lAction = new Runnable() {
		        @Override public void run() {
		        	lHandler.post(this);
		        	//fuel is checked
		        	gView.checkFuel(fuel);
		        	//performs the left thruster method
					gView.leftThruster();
					sThrustSound.start();
					
					//fuel is used
					fuel -= 0.005;
					//fuel gauge is updated
					fuelGauge.setProgress((int)fuel);
					
		        }
		    };
			
			
		});
		
		//reference to the right thruster button
		rThrust = (ImageButton) findViewById(R.id.rightThrustBtn);		
		rThrust.setOnTouchListener(new OnTouchListener(){

			private Handler rHandler;
			
			@Override public boolean onTouch(View v, MotionEvent event) {
		        switch(event.getAction()) {
		        case MotionEvent.ACTION_DOWN:
		            if (rHandler != null) return true;
		            rHandler = new Handler();
		            //performs rAction
		            rHandler.post(rAction);
		            //garbage collection
		            System.gc();
		            System.gc();
		            break;
		        case MotionEvent.ACTION_UP:
		            if (rHandler == null) return true;
		            rHandler.removeCallbacks(rAction);
		            //handler set to null
		            rHandler = null;
		            
		            sThrustSound.pause();
					//sThrustSound.release();
					//sThrustSound = null;
		            //garbage collection
		            System.gc();
		            System.gc();
		            break;
		        }
		        return true;
		    }
			Runnable rAction = new Runnable() {
		        @Override public void run() {
		        	rHandler.post(this);
		        	//check for fuel
		        	gView.checkFuel(fuel);
		        	//perform right thruster method
					gView.rightThruster();
					sThrustSound.start();
					
					//fuel is used
					fuel -= 0.005;
					//fuel gauge is updated
					fuelGauge.setProgress((int)fuel);
					
		        }
		    };
			
			
		});
		
		//reference to main thruster button
		mThrust = (ImageButton) findViewById(R.id.mainThrustBtn);
		/*
		 * ATTEMPT ONE
		mThrust.setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
		            if(fuel > 0){
		            	
		            	gView.checkFuel(fuel);
						gView.mainThruster();
						mThrustSound.start();
						fuel -= 0.5;
						fuelGauge.setProgress((int)fuel);
		            }
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					
					mThrustSound.pause();
					//mThrustSound.release();	
				}
				else{
					//do nothing
				}
				return true;
			}
		});
		*/
		mThrust.setOnTouchListener(new OnTouchListener(){
			private Handler mHandler;
			
			@Override public boolean onTouch(View v, MotionEvent event) {
		        switch(event.getAction()) {
		        case MotionEvent.ACTION_DOWN:
		            if (mHandler != null) return true;
		            mHandler = new Handler();
		            
		            //perform mAction
		            mHandler.post(mAction);
		            //perform garbage collection
		            System.gc();
		            System.gc();
		            break;
		        case MotionEvent.ACTION_UP:
		            if (mHandler == null) return true;
		            mHandler.removeCallbacks(mAction);
		            //handler set to null
		            mHandler = null;
		            mThrustSound.pause();
					//mThrustSound.release();
		            
		            System.gc();
		            System.gc();
		            break;
		        }
		        return true;
		    }
			
			Runnable mAction = new Runnable() {
		        @Override public void run() {
		        	mHandler.post(this);
		        	//check for fuel
		        	gView.checkFuel(fuel);
		        	//perform main thruster method
					gView.mainThruster();
					mThrustSound.start();
					//fuel is used
					fuel -= 0.009;
					//fuel gauge is updated
					fuelGauge.setProgress((int)fuel);
		        }
		    };
			
			
		    /*
		     * ATTEMPT THREE
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int type = event.getAction() & MotionEvent.ACTION_MASK;
				int idx = event.getActionIndex();
				
				Log.e("view",""+v.getId());
				Log.e("type",""+type);
				Log.e("idx",""+idx);
				Log.e("pointers",""+event.getPointerCount());
				
				//if(v.getId() == gView.getId())
				//{
					if(event.getAction() == MotionEvent.ACTION_DOWN)
					{
						gView.checkFuel(fuel);
						gView.mainThruster();
						//mThrustSound.start();
						fuel -= 0.005;
						fuelGauge.setProgress((int)fuel);
					}
					else if(event.getAction() == MotionEvent.ACTION_UP)
					{
						gView.mainThruster();
					}
					return true;
				//}
				//return true;
			}
			*/
		});
		
		
	}
	
	/*
	 * is executed when the application is in a paused state
	 * @see android.app.Activity#onPause()
	 */
	
	@Override
	protected void onPause() {
		super.onPause();
		gView.pause();
	}
	

	/*
	 * is executed when the application is changed from a paused state
	 * to a running state
	 * @see android.app.Activity#onResume()
	 */
	
	@Override
	protected void onResume() {
		super.onResume();
		gView.resume();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	//sets choice of options
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    
			//resets the application
			if(item.getItemId() == R.id.mnuReset)
			{
				Log.e("mnuReset","Reset");
				gView.reset();
				gView.invalidate();
				//fuel is set back to 100
				fuel = 100;
				//fuel gauge is updated
				fuelGauge.setProgress((int)fuel);
			}
			//exits the game
			else if(item.getItemId() == R.id.mnuExit)
			{
				Log.e("mnuExit","finish");
				mThrustSound.setVolume(0, 0);
	        	sThrustSound.setVolume(0, 0);
	        	gView.checkMute(true);
				finish();
				
				
			}
		        
			return super.onOptionsItemSelected(item);
		}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void run() {
		/*
		checkState();
		if(gView.success){
			finalScore = gView.score;
			score.setText(Integer.toString(finalScore));
			Log.e("yes", "checking");
		}
		score.setText(Integer.toString(finalScore));
		*/
	}
	
	//currently not used
	/*
	public void soundFX(){
		sThrustSound = MediaPlayer.create(this, R.raw.secondarythruster);
		sThrustSound.start();
	}
	*/
	/*
	 * not currently working
	public void checkState(){
		if(gView.gameover){
			//finalScore = gView.score;
			//score.setText(Integer.toString(finalScore));
			Log.e("yes", "checking");
		}
	}
	*/

}
