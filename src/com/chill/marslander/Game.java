/*
 * Author: Chris Hill 
 * Class: Game
 * Description: This class is the game class where all the functionality takes place and is the result of the users interaction
 */

package com.chill.marslander;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
//import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
//import android.widget.TextView;

public class Game extends SurfaceView implements Runnable, SurfaceHolder.Callback,
OnTouchListener{

	//global variables
	
	//gravity physics
	private static final int GRAVITY = 2;
	private double v = 1.5;		//velocity
	
	private int width = getContext().getResources().getDisplayMetrics().widthPixels;	//screen width
	private int height = getContext().getResources().getDisplayMetrics().heightPixels;	//screen height
	
	private Canvas canvas;		
	private SurfaceHolder holder;
	
	//private Matrix matrix;				
	private int landerWidth;		//lander width
	private int landerHeight;		//lander height
	private int mThrusterWidth;		//width of thruster bitmap
	
	private float fuel;
	private boolean mute;
	//private int score;
	//TextView finalScore;
	
	//private boolean down;
	private boolean success;
	private boolean gameover;

	private Thread thread = null;
	private boolean safe = false;		//safe to draw
	//private boolean firstRun = true;
	private Random rnd = new Random();
	//private float rotation = 0;			//ship rotation - not used
	private boolean hasFuel = true;
	int starCount = 0;
	
	//A Bitmap object that is going to be passed to the BitmapShader  
	private Bitmap terrainBitmap;
    //The shader that renders the Bitmap  
	private BitmapShader terrainBitmapShader; 
    //the space lander
	private Bitmap lander;
    //main thruster
	private Bitmap mainThruster;
    //secondary/side thrusters
	private Bitmap secThruster;
    
    //lander position
	private float x = width /2; 
	private float y = 10;
    
    //lander collision points
	private float cx;
	private float cy;
	
	private boolean contains;
	
    //terrain
	private Path terrainPath;
	//landing platform
	private Path landingZone;
	
	//x coordinates for terrain
	int xcord[] = { 0, 50, 
			75, 93, 
			113, 126, 139, 147,
			154, 162, 174, 179, 191,
			194, 200, 300,
			325, 335, 343, 
			375,
			384, 397,
			450,
			461, 470, 482, 495, 510, 522,
			532, 547, 560, 572, 581,
			595, 795, // landing
			815, 830, 840, 890, 945, 1015,
			1075, 1103, 1121,
			1171, 1250, 1315,
			1349, 1375,
			1399, 1431, 1486,
			1531, 1620, 1705, 1737, 
						
			 width, width, 0, 0};
	
	//y coordinates for terrain
	int ycord[] = {(height - 150), (height - 220), 
			(height - 200), (height - 187), 
			(height - 198), (height - 237), (height - 241), (height - 241), 
			(height - 234), (height - 218), (height - 171), (height - 171), (height - 197),
			(height - 211), (height - 219), (height - 219), 
			(height - 211), (height - 160), (height - 152),
			(height - 152),
			(height - 147), (height - 120),
			(height - 120),
			(height - 125), (height - 130), (height - 170), (height - 177), (height - 240), (height - 247),
			(height - 240), (height - 177), (height - 170), (height - 130), (height - 125),
			(height - 180), (height - 180), // landing
			(height - 200), (height - 215), (height - 220), (height - 330), (height - 420), (height - 443),
			(height - 415), (height - 409), (height - 397),
			(height - 421), (height - 456), (height - 491),
			(height - 476), (height - 459),
			(height - 481), (height - 495), (height - 501),
			(height - 479), (height - 436), (height - 408), (height - 391),
			height, height, height, height - 150};
	
	int starXCord[] = {(width - width + 150), (width - width + 300), (width - width + 455), (width - width + 591),
			(width - width + 695), (width - width + 853), (width - width + 1015), (width - width + 1258),
			(width - width + 1456), (width - width + 1601), (width - width + 781), (width - width + 1111),
			(width - 256), (width - 50), (width - 150)};
	
	int starYCord[] = {592, 950, 315, 798, 1015, 679, 469, 895, 595, 654, 840, 495, 1301, 751, 365};
				
	//constructor
	public Game(Context context) {
		super(context);
		setup();
	}
	
	//alternative constructor
	public Game(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup();
	}
	//alternative constructor
	public Game(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup();
	}
	//class setup
	public void setup(){
		this.isInEditMode();	//eclipse use
		
		setOnTouchListener(this);
		
		terrainPath = new Path();
		landingZone = new Path();
		//matrix = new Matrix();	//not used
		
		//define lander bitmap
		lander = BitmapFactory.decodeResource(getResources(), R.drawable.lander);
		//get lander bitmap width
		landerWidth = lander.getWidth();
		//get lander bitmap height
		landerHeight = lander.getHeight();
		//define main thruster bitmap
		mainThruster = BitmapFactory.decodeResource(getResources(), R.drawable.mainbooster);
		//define main thruster bitmap width
		mThrusterWidth = mainThruster.getWidth();
		//define secondary thruster bitmap
		secThruster = BitmapFactory.decodeResource(getResources(), R.drawable.booster);
		
		//define terrain bitmap
		terrainBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.terrain);
		//Bitmap resizedBitmap = Bitmap.createScaledBitmap(terrainBitmap , 50, 50, false);
		
		//repeat the defined terrain bitmap
        terrainBitmapShader = new BitmapShader(terrainBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        
        //connect the terrain path points
		for (int i = 0; i < xcord.length; i++) {
			terrainPath.lineTo(xcord[i], ycord[i]);
		}
		
		//connect the landing zone points
		landingZone.moveTo(xcord[34], ycord[34]);
		landingZone.lineTo(xcord[35], ycord[35]);
		landingZone.close();
		
		// holder is given the surface view's surface
		holder = getHolder();
		
		getHolder().addCallback(this);
		
		//finalScore = (TextView) findViewById(R.id.score);
	}
	

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
	}

	/*
	 * executed when the thread is running
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
			while (safe == true){
				while(!gameover){
				//check if it's safe to draw
				if(!holder.getSurface().isValid()){
					continue;
				}
				
				synchronized(holder){
					//lock canvas for drawing
					canvas = holder.lockCanvas();
					//draw background color - black
					canvas.drawColor(Color.BLACK);
					//draw background
					
					Paint white = new Paint();
					white.setColor(Color.WHITE);
					/*
					while(starCount < 15){
						canvas.drawCircle(a, b, 10, white);
					
						starCount++;
						
					}*/
					
					for (int i = 0; i < starXCord.length; i++) {
						
						canvas.drawCircle(starXCord[i], starYCord[i], 5, white);
					}
					
					
					//style for landing platform
					Paint strokePaint = new Paint();  
			        strokePaint.setDither(true);  
			        strokePaint.setColor(Color.GREEN);  
			        strokePaint.setStyle(Paint.Style.STROKE);  
			        strokePaint.setAntiAlias(true);  
			        strokePaint.setStrokeWidth(5);
			        
					//fill terrain path style
					Paint terrainBG = new Paint();
					terrainBG.setColor(0xFFFFFFFF);  
					terrainBG.setStyle(Paint.Style.FILL);
					terrainBG.setShader(terrainBitmapShader);
			        
					//draw the terrain
					canvas.drawPath(terrainPath, terrainBG);
					terrainPath.close();
					//draw the landing platform
					canvas.drawPath(landingZone, strokePaint );
					
					//rotation of lander - not used
					//matrix.postRotate(rotation);
					
					//Bitmap newLander = Bitmap.createBitmap(lander, 0, 0,		not used - used for rotation
		            //        landerWidth, landerHeight, matrix, true);
					
					
					
					//draw the lander
					canvas.drawBitmap(lander, x - (landerWidth/2), y - (landerHeight/2), null);
					
					//call collision method - check if collide with terrain
					collisionDetection();
					
					//wrap around terrain
					if(x - (landerWidth/2) > width){		//if greater than width, teleport to other side
						x = 0;
					}
					else if (x + (landerWidth/2) < 0){		//if less than 0, teleport to other side
						x = width;
					}
					else{
						
					}
					//if(y < 0 - landerHeight){				//if too high, slightly increase fall rate
					//	reset();
					//}
					
					//gravity for lander
					y = (int) y + (int) (v + (0.5 * (GRAVITY * v * v)));
					//fall speed
					v = v + 0.01;			
					
					//if lander does collide
					if(contains)
					{
						//check if it was a good collide - landing platform
						if(success){
							//calculateScore();
							//end game - stop movement													
							gameover = true;
							try {
								Thread.sleep(500);
								} catch (InterruptedException e) {
								 e.printStackTrace();
								}
						}
						//if not a good collide - crash
						else{
							//define crash explosion bitmap
							Bitmap crash = BitmapFactory.decodeResource(getResources(), R.drawable.explosion);
							//draw bitmap at crash site
							canvas.drawBitmap(crash, cx - (crash.getWidth()/2), y - (crash.getHeight()/2), null);
							//Log.e("col", "hit");
							//end game - stop movement
							
							MediaPlayer explosion = MediaPlayer.create(getContext(), R.raw.explosion);
							if(!mute){
								explosion.start();
							}
							gameover = true;
							try {
								Thread.sleep(500);
								System.gc();
								} catch (InterruptedException e) {
								 e.printStackTrace();
								}
						}
					}	
				}
				
				//unlock the canvas and display
				holder.unlockCanvasAndPost(canvas);
			
				try {
					Thread.sleep(25);
					} catch (InterruptedException e) {
					 e.printStackTrace();
					}
				
				//garbage collection
				System.gc();
				System.gc();
				}
			}
	}
	
	
	/***
	 * contains method
	 * checks if the lander x and y position intersects with the x and y coordinates for the terrain
	 * if yes and not on landing platform then it is a crash and contains is set to true
	 * if yes and it is on landing platform then it is a successful collision
	 * @param xcord
	 * @param ycord
	 * @param x0
	 * @param y0
	 * @return
	 */
	public boolean contains(int[] xcord, int[] ycord, double x0, double y0) {
		int crossings = 0;

		for (int i = 0; i < xcord.length - 1; i++)
		{
			int x1 = xcord[i];
			int x2 = xcord[i + 1];

			int y1 = ycord[i];
			int y2 = ycord[i + 1];

			int dy = y2 - y1;
			int dx = x2 - x1;

			double slope = 0;
			if (dx != 0) {
				slope = (double)dy / dx;
				//Log.e("slope",Double.toString(slope)+"["+i+"]");
			}

			boolean cond1 = (x1 <= x0) && (x0 < x2); 		// range
			boolean cond2 = (x2 <= x0) && (x0 < x1); 		// reverse range
															
			boolean above = (y0 < slope * (x0 - x1) + y1);  //below path

			//records collision
			if ((cond1 || cond2) && above) {
				crossings++;
			}
			//records if it was a good collision
			if ((cx > xcord[34]) && (cx < xcord[35])){
				success = true;
			}
		}
		return (crossings % 2 != 0); // even or odd
	}
	
	/***
	 * collision detection
	 * sets contains to true if there is a collision
	 */
	public void collisionDetection(){
		cy = y + (landerHeight/2);
		cx = x;
		
		contains = contains(xcord, ycord, cx, cy);
	}
	
	//executed on pause
	public void pause() {
		safe = false;
		while(true){
			try{
				thread.join();
			}
			catch (InterruptedException e){
				e.printStackTrace();
			}
			break;
		}
		thread = null;
	}
	
	//executed on resume
	public void resume() {
		safe = true;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

	}

	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}
	
	/***
	 * main thruster method
	 * checks if there is fuel
	 * updates lander position
	 * draws the thruster bitmap
	 */
	public void mainThruster(/*boolean newDown*/){
		if(hasFuel == true){
			//updates y position
			y -= 0.25;
			//prepare
			mainThruster.prepareToDraw();
			//draws bitmap
			canvas.drawBitmap(mainThruster, x - (mThrusterWidth / 2), y + 15  , null);
		}
		else{
			y -= 0;
		}
	}
	
	/***
	 * left thruster method
	 * checks if there is fuel
	 * updates lander position
	 * draws the thruster bitmap
	 */
	public void leftThruster(){
		if(hasFuel == true){
			//updates x position - moves lander right
			x -= 0.05;
			//no rotation
			//rotation -= 0.001;
			
			//prepare
			secThruster.prepareToDraw();
			//draw bitmap
			canvas.drawBitmap(secThruster, x - (mThrusterWidth / 2) + (landerWidth /2) - 15, y - 35, null);
		}else{
			x -= 0;
		}
	}
	
	/***
	 * right thruster method
	 * checks if there is fuel
	 * updates lander position
	 * draws the thruster bitmap
	 */
	public void rightThruster(){
		if(hasFuel == true){
			//updates x position - moves lander left
			x += 0.05;
			//no rotation
			//rotation += 0.001;
			
			//prepare
			secThruster.prepareToDraw();
			//draw bitmap
			canvas.drawBitmap(secThruster, x - (mThrusterWidth / 2) - (landerWidth /2) + 20, y - 35, null);
		}else{
			x += 0;
		}
	}
	
	/***
	 * reset method
	 * resets lander position to a random x coordinate on screen
	 * reset gravity fall velocity
	 * gameover set to false
	 * fuel is refilled
	 * surface is safe to draw
	 * success is set to false
	 * first run set to true
	 * garbage collection
	 */
	public void reset()
	{	
		x = rnd.nextInt(width);
		y = 10;
		v = 1.5;
		//rotation = (float) 0.0000;		// not used
		gameover = false;
		hasFuel = true;
		safe = true;
		success = false;
		//firstRun = true;
		System.gc();
		
	}
	
	/***
	 * check fuel
	 * check if fuel is not equal to zero
	 * if no then the ship has fuel
	 * else then the ship has no fuel and cannot move
	 * @param newFuel
	 */
	public void checkFuel(float newFuel){
		fuel = newFuel;
		if(fuel <= 0){
			hasFuel = false;
			//Log.e("fuel", "false");
		}else{
			hasFuel = true;
			//Log.e("fuel", "true");
		}
	}
	
	/***
	 * checks to see if mute button has been pushed
	 * sets mute to given boolean value
	 * @param newMute
	 */
	public void checkMute(boolean newMute){
		mute = newMute;
	}
	
	/***
	 * calculate score method
	 * if land on pad the the score is calculated by how close the lander is to the center
	 */
	/*
	 * could not fully get working
	public void calculateScore(){
		int num;
		
		float cord1 = 595;//xcord[34];
		float cord2 = 795;//xcord[35];
		if(x > cord1 + 100){					//greater than x cord
			num = (int) (cord2 - x);	//xcord - lander position
			score = (int) (cord2 - num);		//x cord - num to get the lander x which is the score
		}
		else{
			num = (int) (cord2 - x);	//xcord - lander position
			score = (int) (cord2 - num);		//x cord - num to get the lander x which is the score
		}
		
		
			//String str = Interger.(score);
			//set the displayed text to the score
			//finalScore.setText(Integer.toString(score));
			//gameover = true;
		
			
	}*/
	
	/*public void createLBooster(){
		secThruster = BitmapFactory.decodeResource(getResources(), R.drawable.booster);
		mThrusterWidth = mainThruster.getWidth();
		mThrusterHeight = mainThruster.getHeight();
		canvas.drawBitmap(secThruster, x - (mThrusterWidth / 2) - (landerWidth /2) + 20, y - 35, null);
		secThruster.recycle();
	}*/
	
	/*public void createRBooster(){
		secThruster = BitmapFactory.decodeResource(getResources(), R.drawable.booster);
		mThrusterWidth = mainThruster.getWidth();
		mThrusterHeight = mainThruster.getHeight();
		canvas.drawBitmap(secThruster, x - (mThrusterWidth / 2) + (landerWidth /2) - 20, y - 35, null);
		//secThruster.recycle();
	}*/
	
	/*public void createMBooster(){
		mainThruster = BitmapFactory.decodeResource(getResources(), R.drawable.mainbooster);
		mThrusterWidth = mainThruster.getWidth();
		mThrusterHeight = mainThruster.getHeight();
		canvas.drawBitmap(mainThruster, x - (mThrusterWidth / 2), y + 15  , null);
		mainThruster.recycle();
	}*/
	
	/***
	 * background method
	 * draws stars in random locations on screen on the first run of each game
	 * then the first run is set to false so that it is not run again
	 */
	public void background(){
		Paint white = new Paint();
		white.setColor(Color.WHITE);
		while(starCount < 15){
			Log.e("dfgdf", "drawfucker");
			float starX = canvas.getWidth() - 103;
			float starY = canvas.getHeight()- 57;
			canvas.drawCircle(starX, starY, 1, white);
			starCount++;
			starX -= 79;
			starY -= 63;
		}
		//firstRun = true;
	}
}


